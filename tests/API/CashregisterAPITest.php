<?php
//    Pasteque server testing
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pasteque.
//
//    Pasteque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pasteque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pasteque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque\Server;

use \Pasteque\Server\API\CashregisterAPI;
use \Pasteque\Server\Model\CashRegister;
use \Pasteque\Server\System\DAO\DAOFactory;
use \PHPUnit\Framework\TestCase;

require_once(dirname(dirname(__FILE__)) . "/common_load.php");

class CashregisterAPITest extends TestCase
{
    private $dao;
    private $api;

    protected function setUp(): void {
        global $dbInfo;
        $this->dao = DAOFactory::getDAO($dbInfo, ['debug' => true]);
        $this->api = new CashregisterAPI($this->dao);
    }

    protected function tearDown(): void {
        $all = $this->dao->search(CashRegister::class);
        foreach($all as $record) {
            $this->dao->delete($record);
        }
        $this->dao->commit();
        $this->dao->close();
    }

    /**
     * Create a cash register with a preset next ticket id with write
     * and check that the balance is still 0.
     */
    public function testWriteCreateNextTicketId() {
        $c = new CashRegister();
        $c->setReference("cr");
        $c->setLabel("Cash register");
        $c->setNextTicketId(5);
        $this->api->write($c);
        $snapshot = $this->dao->readSnapshot(CashRegister::class, $c->getId());
        $this->assertNotNull($c);
        $this->assertEquals(1, $snapshot->getNextTicketId());
        $this->assertEquals("Cash register", $snapshot->getLabel());
    }

    /** @depends testWriteCreateNextTicketId
     * Check that writing a cash register with an existing next ticket id
     * does not modify it.
     */
    public function testWriteUpdateNextTicketId() {
        $c = new CashRegister();
        $c->setReference("cr");
        $c->setLabel("Cash register");
        $c->setNextTicketId(5);
        $this->dao->write($c); // force update by skipping API
        $this->dao->commit();
        $c->setLabel("New label");
        $c->setNextTicketId(10);
        $this->api->write($c);
        $snapshot = $this->dao->readSnapshot(CashRegister::class, $c->getId());
        $this->assertNotNull($c);
        $this->assertEquals(5, $snapshot->getNextTicketId());
        $this->assertEquals("New label", $snapshot->getLabel());
    }
}
