<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Server\API;

use \Pasteque\Server\System\DAO\DAOCondition;

/** CRUD API for CashRegister (attention: lowercase r in name).  */
class CashregisterAPI extends APIRefHelper
{
    const MODEL_NAME = 'Pasteque\Server\Model\CashRegister';

    public function getByName($name) {
        $registers = $this->dao->search(static::MODEL_NAME,
                new DAOCondition('label', '=', $name), 1);
        if (count($registers) > 0) {
            return $registers[0];
        }
        return null;
    }

    /**
     * Write one or multiple cash registers. Next ticket id cannot be updated
     * this way to prevent unexpected changes. It is updated when
     * writing tickets.
     */
    public function write($data) {
        $this->supportOrDie($data);
        $arrayArgs = is_array($data);
        $data = ($arrayArgs) ? $data : array($data);
        for ($i = 0; $i < count($data); $i++) {
            $d = $data[$i];
            if ($d->getId() === null) {
                // New record, force initial next ticket id
                $d->setNextTicketId(1);
            }
            $snapshot = $this->dao->readSnapshot(static::MODEL_NAME,
                    $d->getId());
            if ($snapshot !== null) {
                // Found previous record, pick it's next ticket id.
                $d->setNextTicketId($snapshot->getNextTicketId());
            } else {
                // Old record not found, set initial value in case...
                $d->setNextTicketId(1);
            }
            $data[$i] = $d;
        }
        return parent::write(($arrayArgs) ? $data : $data[0]);
    }

}
