<?php

use \Pasteque\Server\Model\FiscalTicket;

function render($ptApp, $data) {
    $ret = sprintf("<p>Connecté·e en tant que %s. <a href=\"./disconnect\">Déconnexion</a></p>", $data['user']);
    $ret .= '<h1>Consultation des données fiscale</h1>';
    $ret .= '<h2>Lister tous les tickets par caisse</h2>';
    $ret .= '<ul>';
    foreach ($data['sequences'] as $sequence) {
        $ret .= '<li>' . htmlspecialchars($sequence) . ' : <a href="./sequence/' . htmlspecialchars($sequence) . '/z/" target="_blank">Tickets Z</a> <a href="./sequence/' . htmlspecialchars($sequence) . '/tickets/" target="_blank">Tickets</a>';
        foreach ($data['types'] as $customType) {
            if ($customType != FiscalTicket::TYPE_ZTICKET
                    && $customType != FiscalTicket::TYPE_TICKET) {
                $ret .= ' <a href="./sequence/' . htmlspecialchars($sequence) . '/other?type=' . htmlspecialchars($customType) . '" target="_blank">' . htmlspecialchars($customType) . '</a>';
            }
        }
        $ret .= '</li>';
    }
    $ret .= '</ul>';
    if (!empty($data['gpg'])) {
        $ret .= '<h2>Archives</h2>';
        if (count($data['archives']) > 0) {
            $ret .= '<h3>Archives disponibles</h3>';
            $ret .= "<p>Les archives sont à conserver pendant au moins 6 ans à partir de la date de la dernière opération. Veillez à en conserver une ou plusieurs copies en endroit sûr. Celle-ci sont signées via le standard OpenPGP pour assurer leur non altérabilité. Pour lire une archive, déchiffrez là à l'aide d'un client PGP (tel que <a href=\"https://gnupg.org\">GnuPG</a>) avec la clé publique que vous a fourni votre prestataire Pastèque. Conservez l'archive signée ainsi que la clé publique pour garantir l'intégrité par rapport à l'archive originale.</p>";
            $ret .= '<ul>';
            foreach ($data['archives'] as $archive) {
                $num = $archive->get('number');
                if ($num == 0) {
                    continue;
                }
                $info = $archive->get('info');
                $name = "";
                if ($info->get('name') != null) {
                    $name = $info->get('name');
                } elseif (($info->get('dateStart') != null)
                        && ($info->get('dateStop') != null)) {
                    $start = \DateTime::createFromFormat('Y-m-d H:i:s',
                            $info->get('dateStart'));
                    $stop = \DateTime::createFromFormat('Y-m-d H:i:s',
                            $info->get('dateStop'));
                    $name = sprintf('du %s au %s',
                            $start->format('d/m/Y H:i:s'),
                            $stop->format('d/m/Y H:i:s'));
                }
                $generated = "";
                if ($info->get('generated') != null) {
                    $genDate = \DateTime::createFromFormat('Y-m-d H:i:s',
                            $info->get('generated'));
                    $generated = sprintf('créée le %s à %s',
                            $genDate->format('d/m/Y'),
                            $genDate->format('H:i:s'));
                }
                $ret .= sprintf('<li><a href="./archive/%d">%d - %s</a> %s</li>',
                        $num, $num, $name, $generated);
            } // TODO: add signature check
            $ret .= '</ul>';
        } else {
            $ret .= "<p>Aucune archive n'a été générée.</p>";
        }
        $ret .= '<h3>Archives en cours de création</h3>';
        if (count($data['archiverequests']) > 0) {
            $ret .= '<ul>';
            foreach ($data['archiverequests'] as $ar) {
                $ret .= '<li>' . $ar->getId() . ' - du ' . $ar->getStartDate()->format('d/m/Y H:i:s') . ' au ' . $ar->getStopDate()->format('d/m/Y H:i:s');
            }
            $ret .= '</ul>';
        } else {
            $ret .= "<p>Aucune archive n'est en attente de création.</p>";
        }
        $ret .= '<h3>Créer une nouvelle archive</h3>';
        $ret .= "<p>Choisissez la période à archiver et appuyez sur \"Envoyer\". Une demande de création d'archive sera enregistrée. L'archive elle-même sera disponible plus tard. Contactez votre prestataire Pastèque pour plus d'information sur les délais de génération des archives.</p>";
        $ret .= "L'archive ne peut couvrir plus d'une année.</p>";
        $ret .= '<form action="createarchive" method="post">';
        $ret .= '<label for="period-from">Depuis le </label><input type="date" name="dateStart" id="period-from" placeholder="aaaa/mm/jj" required="true" /> ';
        $ret .= "<label for=\"period-to\">jusqu'au </label><input type=\"date\" name=\"dateStop\" id=\"period-to\" placeholder=\"aaaa/mm/jj\" required=\"true\" /> <input type=\"submit\" value=\"Envoyer\" />";
        $ret .= '<p>Les dates correspondent aux date de reversement des tickets. Si une caisse est hors ligne à la date de fin de la période, ses tickets seront intégrés dans une archive qui couvrira la date du rétablissement de connexion.</p>';
        $ret .= '</form></li>';
    }
    $ret .= '<h2>Export des tickets</h2>';
    $ret .= "<p>L'export des tickets vous permet de transférer une copie sur un miroir. Pour la conservation de vos données, utilisez une archive.</p>";
    $ret .= '<ul>';
    $ret .= '<li><a href="./export?period=P7D">Exporter une semaine</a></li>';
    $ret .= '<li><a href="./export?period=P14D">Exporter deux semaines</a></li>';
    $ret .= '<li><a href="./export?period=P1M">Exporter un mois</a></li>';
    $ret .= '<li><form action="export" method="get">';
    $ret .= '<label for="period-from">Exporter depuis le </label><input type="date" name="from" id="period-from" placeholder="aaaa/mm/jj" required="true" /> <input type="submit" value="Exporter" />';
    $ret .= '</form></li>';
    $ret .= '<li><form action="export" method="get">';
    $ret .= '<label for="interval-from">Exporter depuis le </label><input type="date" name="from" id="interval-from" placeholder="aaaa-mm-jj" required="true" /> <label for="interval-to">au </label><input type="date" name="to" id="interval-to" placeholder="aaaa-mm-jj" required="true" /> <input type="submit" value="Exporter" />';
    $ret .= '</form></li>';
    $ret .= '</ul>';
    if ($ptApp->isFiscalMirror()) {
        $ret .= '<h2>Import des tickets</h2>';
        $ret .= '<p>Exportez les tickets depuis votre instance d\'usage pour en reverser une copie sur ce miroir. Les tickets déjà importés seront ignorés.</p>';
        $ret .= '<p><form method="POST" action="import" enctype="multipart/form-data">';
        $ret .= '<input type="file" name="file" required="true" /> <input type="submit" value="Envoyer" /></form></p>';
    }
    $ret .= '<h2>Aide</h2>';
    $ret .= '<ul>';
    $ret .= '<li><a href="./help/tickets" target="_blank">Description des champs des tickets</a></li>';
    $ret .= '<li><a href="./help/archives" target="_blank">Archives</a></li>';
    $ret .= '<li><a href="./help/issues" target="_blank">Problèmes connus</a></li>';
    $ret .= '</ul>';
    return $ret;
}
