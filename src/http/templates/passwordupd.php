<?php

function render($ptApp, $data) {
    $ret = <<<HTML
<div id="content" class="container-fluid">
  <h1 style="text-align:center; padding-top: 8%">Pastèque<br />Création d'une empreinte de mot de passe</h1>
  <div id="login" style="margin: auto; width: 50ex">
    <form name="loginform" id="loginform" action="." method="post" style="background-color: #fff; box-shadow: 0 1px 3px #777; padding: 1ex;">
      <p>
        <label for="pwd1">Mot de passe<br />
          <input type="password" required="true" name="password1" id="pwd1" class="input" value="" size="20" /></label>
      </p>
      <p>
        <label for="pwd2">Confirmer le mot de passe<br />
          <input type="password" required="true" name="password2" id="pwd2" class="input" value="" size="20" /></label>
      </p>
HTML;
    if (!empty($data['error'])) {
        $ret .= "<p class=\"feedback\">";
        $ret .= htmlspecialchars($data['error']);
        $ret .= "</p>";
    }
    $ret .= <<<HTML
      </p>
      <p class="submit">
        <input type="submit" name="submit" id="submit" class="button" value="Générer" />
      </p>
    </form>
  </div>
HTML;
    if (!empty($data['hash'])) {
        $ret .= "<p style=\"text-align:center;\">L'empreinte du mot de passe saisi est la suivante, vous pouvez l'envoyer à votre prestataire pour enregistrement :<br>" . htmlspecialchars($data['hash']) . '</p>';
    }
    $ret .= <<<HTML
</div>
HTML;
    return $ret;
}
