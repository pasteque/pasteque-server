<?php

function render($ptApp, $data) {
    $tpl = <<<HTML
<div id="content" class="container-fluid">
  <div id="home" style="margin: auto; width: 35ex; padding-top: 8%; background-color: #fff; box-shadow: 0 1px 3px #777; padding: 1ex;">
    <h1 style="text-align:center;">API Pastèque</h1>
    <ul style="list-style-type: none; padding: 1ex;">
      <li style="margin-bottom: 2ex;"><a href="./fiscal/">Consulter vos enregistrements fiscaux et archives</a></li>
      {{backoffice}}
      <li style="margin-top: 4ex; margin-bottom: 2ex;"><a href="./passwordupd/">Générer une empreinte de mot de passe</a></li>
    </ul>
  </div>
</div>
HTML;
    $bo = $ptApp->getDefaultBackOfficeUrl();
    if ($bo && !$ptApp->isFiscalMirror()) {
        $tpl = str_replace('{{backoffice}}',
                sprintf("<li style=\"margin-bottom: 2ex\"><a href=\"%s\">Accéder à l'interface de gestion suggérée</a></li>", htmlspecialchars($bo, ENT_QUOTES)),
                $tpl);
    } else {
        $tpl = str_replace('{{backoffice}}', '', $tpl);
    }
    return $tpl;
}
