<?php

use \Pasteque\Server\System\DateUtils;

function renderTicket($tkt) {
    $data = json_decode($tkt['content']);
    $output = ($tkt['content']);
    if ($data !== null) {
        $output = json_encode($data, \JSON_PRETTY_PRINT);
    }
    $ret = '<div>';
    $ret .= '<h3>Numéro : ' . $tkt['number'] . '</h3>';
    $ret .= '<p>Signature : ' . $tkt['signature_status'] . ' <span style="font-size:x-small">' . htmlspecialchars($tkt['signature']) . '</span><br>';
    $ret .= "Date d'enregistrement : " . (DateUtils::readDate($tkt['date'])->format('d/m/Y H:i:s')) . '</p>' ;
    $ret .= '<pre>' . htmlspecialchars($output) . '</pre>';
    $ret .= '</div>';
    return $ret;
}

function renderPagination($data) {
    if ($data['pageCount'] == 0) {
        return '';
    }
    $ret = '<p>Page</p>';
    $ret .= '<p>';
    // First page
    if ($data['pageCount'] > 0) {
        if ($data['page'] == 0) {
            $ret .= '0 ';
        } else {
            $ret .= '<a href=".?page=0">0</a> ';
        }
    }
    // Other pages
    for ($i = 1; $i < $data['pageCount'] - 1; $i++) {
        if (abs($i - $data['page']) < 10 || $i % 50 == 0) {
            if ($data['page'] == $i) {
                $ret .= $i . ' ';
            } else {
                $ret .= '<a href=".?page=' . htmlspecialchars($i) . '">' . htmlspecialchars($i) . '</a> ';
            }
        }
    }
    // Last page
    if ($data['pageCount'] > 1) {
        if ($data['page'] == $data['pageCount'] - 1) {
            $ret .= $data['page'];
        } else {
            $ret .= '<a href=".?page=' . htmlspecialchars($data['pageCount'] - 1) . '">' . htmlspecialchars($data['pageCount'] - 1) . '</a> ';
        }
    }
    $ret .= '</p>';
    if ($data['pageCount'] <= 1) {
        return $ret;
    }
    // Prev/next links
    $hasPrev = $data['page'] != 0;
    $hasNext = $data['page'] != $data['pageCount'] - 1;
    if ($hasPrev || $hasNext) {
        $ret .= '<p>';
        if ($hasPrev) {
            $ret .= '<a href=".?page=' . htmlspecialchars($data['page'] - 1) . '">Page précédente</a> ';
        }
        if ($hasNext) {
            $ret .= '<a href=".?page=' . htmlspecialchars($data['page'] + 1) . '">Page suivante</a> ';
        }
        $ret .= '</p>';
    }
    return $ret;
}

function render($ptApp, $data) {
    $ret = '<h2>Liste des ' . htmlspecialchars($data['typeName']) . '</h2>';
    if (count($data['tickets']) == 0) {
        $ret .= '<p>Aucun enregistrement</p>';
    }
    $ret .= renderPagination($data);
    foreach ($data['tickets'] as $tkt) {
        $ret .= renderTicket($tkt);
    }
    $ret .= renderPagination($data);
    return $ret;
}
