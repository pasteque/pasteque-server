<?php

function render($ptApp, $data) {
    return <<<HTML
<div id="content" class="container-fluid">
  <div id="login" style="margin: auto; width: 30ex; padding-top: 8%;">
    <h1>Pastèque<br />Login</h1>
    <form name="loginform" id="loginform" action="." method="post" style="background-color: #fff; box-shadow: 0 1px 3px #777; padding: 1ex;">
      <p>
        <label for="user_login">Identifiant<br />
          <input type="text" name="user" id="user_login" class="input" value="" size="20" /></label>
      </p>
      <p>
        <label for="user_pass">Mot de passe<br />
          <input type="password" name="password" id="user_pass" class="input" value="" size="20" /></label>
      </p>
      <p class="submit">
        <input type="submit" name="submit" id="submit" class="button" value="Se connecter" />
      </p>
    </form>
  </div>
</div>
HTML;
}
